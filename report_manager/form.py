from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import Report, emp_type


class SignupForm(UserCreationForm):
    email = forms.CharField(
        max_length=100, required=True, widget=forms.EmailInput)
    type = forms.ChoiceField(choices=emp_type)

    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'type']


class ReportForm(forms.ModelForm):
    class Meta: 
        model = Report
        fields = ['title', 'info', 'user_name']

class UpdateReportForm(forms.ModelForm):
    class Meta: 
        model = Report
        fields = ['status']


