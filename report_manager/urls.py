from django.urls import path, include
from . import views
# from django.contrib.auth import views as auth_view
# from django.contrib.auth.decorators import login_required


urlpatterns = [
    path('signup/', views.SignUp.as_view()),
    path('create/', views.CreateReport.as_view()),
    path('review/<int:report_id>', views.review_report),
    path('update/<int:report_id>', views.leader_action),
    path('report', views.ReportList.as_view()),
    path('report/<int:pk>', views.ReportDetails.as_view()),
    path('employee/<int:pk>', views.UserDetail.as_view()),
    path('employee/', views.UserList.as_view()),
    path('api-auth/', include('rest_framework.urls')),


    # path('logout/', auth_view.LogoutView.as_view()),
    # path('login/', auth_view.LoginView.as_view()),
    # path('change_password/', auth_view.PasswordChangeView.as_view()),
    # path('change_password/', auth_view.PasswordChangeDoneView.as_view()),
    # path('change_password/done/', auth_view.PasswordChangeDoneView.as_view()),
    # path('profile/', login_required(views.GetProfile.as_view()))
]
