# Generated by Django 3.1.4 on 2021-01-24 11:02

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('report_manager', '0012_auto_20210124_1011'),
    ]

    operations = [
        migrations.AlterField(
            model_name='report',
            name='employees',
            field=models.ManyToManyField(related_name='emp', through='report_manager.ReportProcess', to=settings.AUTH_USER_MODEL),
        ),
    ]
