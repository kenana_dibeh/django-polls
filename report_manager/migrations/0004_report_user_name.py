# Generated by Django 3.1.4 on 2021-01-20 09:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report_manager', '0003_auto_20210120_0645'),
    ]

    operations = [
        migrations.AddField(
            model_name='report',
            name='user_name',
            field=models.CharField(default='kenana', max_length=20),
            preserve_default=False,
        ),
    ]
