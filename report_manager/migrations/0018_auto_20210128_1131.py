# Generated by Django 3.1.4 on 2021-01-28 11:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('report_manager', '0017_auto_20210128_0747'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='role',
            field=models.CharField(blank=True, choices=[('client', 'Client'), ('employee', 'Employee'), ('leader', 'Leader')], max_length=20, null=True),
        ),
        migrations.AlterField(
            model_name='reportprocess',
            name='emp_type',
            field=models.CharField(choices=[('client', 'Client'), ('employee', 'Employee'), ('leader', 'Leader')], max_length=10),
        ),
    ]
