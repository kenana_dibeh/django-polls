# Generated by Django 3.1.4 on 2021-01-21 11:00

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('report_manager', '0005_auto_20210121_0636'),
    ]

    operations = [
        migrations.AddField(
            model_name='reportprocess',
            name='emp_type',
            field=models.CharField(choices=[('employee', 'Employee'), ('leadeer', 'Leader')], default='employee', max_length=10),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='reportprocess',
            name='employee',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
