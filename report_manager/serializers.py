from django.contrib.auth.models import User as Employee
from .models import Report, ReportProcess
from rest_framework import serializers


class ReportSerializer(serializers.HyperlinkedModelSerializer):
    process = serializers.StringRelatedField(many=True)
    owner = serializers.ReadOnlyField(source='owner.username')


    class Meta: 
        model = Report
        fields = ['title', 'info', 'user_name', 'process', 'owner']


class EmployeeSerializer(serializers.ModelSerializer):
    process = serializers.PrimaryKeyRelatedField(many=True, queryset=ReportProcess.objects.all())

    class Meta:
        model = Employee
        fields = ['id', 'username', 'process']