from django.db import models
from django.contrib.auth.models import User as Employee



status_choices = [
    ('submitted', 'Submitted'),
    ('review_accepted', 'Review_Accepted'),
    ('review_unaccepted', 'Review_unaccepted'),
    ('approval', 'Approval'),
    ('rejected', 'Rejected'),
]
emp_type = [
    ('client', 'Client'),
    ('employee', 'Employee'),
    ('leader', 'Leader'),
]

class Report(models.Model):
    user_name = models.CharField(max_length=20)
    title = models.CharField(max_length=50)
    info = models.TextField(max_length=200)
    status = models.CharField(max_length=20, choices=status_choices, default='submitted')
    publish_at = models.DateField(auto_now=True)
    accepting_count = models.IntegerField(default=0)
    employees = models.ManyToManyField(Employee,through='ReportProcess')

    def __str__(self):
        return self.title
    
    class Meta:
        permissions = [
            ("can_create", "Can create new report"),
            ("can_review", "Can review the report"),
            ("can_done_action", "Can do the done action"),
        ]

class ReportProcess (models.Model):
    employee = models.ForeignKey(Employee,on_delete=models.CASCADE, null=True, related_name='process')
    report = models.ForeignKey(Report,on_delete=models.CASCADE, null=False, related_name= 'process')
    emp_type = models.CharField(max_length=10,choices=emp_type )
    publish_at = models.DateField(auto_now=True)

    def __str__(self):
        return f'({self.report}) updated by the {self.emp_type}: {self.employee}'


class Profile(models.Model):
    user = models.OneToOneField(Employee, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)
    birthdate = models.DateField(null=True, blank=True)
    role = models.CharField(max_length=20,choices=emp_type, null=True, blank=True)
    
    def __str__(self):
        return 'Profile of user: {}'.format(self.user.username)

