from django.contrib import admin

# Register your models here.
from .models import Employee, Report, ReportProcess,Profile

admin.site.register(Report)
admin.site.register(ReportProcess)
admin.site.register(Profile)



