from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from rest_framework import viewsets
from .models import Report, ReportProcess, Profile, emp_type
from django.contrib.auth.models import User as Employee
from django.contrib.auth import login, logout
from django.contrib.auth.forms import UserCreationForm
from .form import SignupForm, ReportForm, UpdateReportForm
from django.views.generic import View, FormView, DetailView, CreateView, UpdateView, ListView
from .serializers import ReportSerializer, EmployeeSerializer
from rest_framework import generics
from rest_framework import permissions
from django.contrib.auth.models import Permission
from django.contrib.auth.decorators import permission_required, login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.mixins import PermissionRequiredMixin



#  --- Report ---
class SignUp(FormView):
    form_class = SignupForm
    template_name = 'registration/signup.html'
    success_url = 'http://127.0.0.1:8000/report_manager/report'

    def form_valid(self, form):
        user = form.save()
        user.is_staff= True
        type = self.request.POST.get('type')
        content_type = ContentType.objects.get_for_model(Report)
        create_per = Permission.objects.get(codename='can_create', content_type=content_type)
        review_per = Permission.objects.get(codename='can_review', content_type = content_type)
        action_per = Permission.objects.get(codename='can_done_action', content_type=content_type)
        user.save()
        if type == emp_type[0][0]: 
            user.user_permissions.add(create_per)
            user.save()

            return super().form_valid(form) 
        elif type == emp_type[1][0]:
            user.user_permissions.add(review_per)
            user.save()
        elif type == emp_type[2][0]:
            user.user_permissions.add(action_per)
            user.save()
        else:
            user.delete()
            return HttpResponse('<br> <h2>Error: insert your type !!</h2>')

        Profile.objects.create(
            user = user,
            name= user.username,
            role = type
        )
        login(self.request, user)
        return super().form_valid(form)

class CreateReport(PermissionRequiredMixin, CreateView):
    form_class = ReportForm
    template_name = 'report/create_report.html'
    # success_url = f'http://127.0.0.1:8000/report_manager/report/{}'
    permission_required = 'report_manager.can_create'


@login_required
@permission_required('report_manager.can_review',raise_exception=True, login_url='http://127.0.0.1:8000/admin/login/?next=/admin/')
def review_report(request, report_id):
    report = Report.objects.get(id=report_id)
    new_status = request.POST.get("status")

    if request.method == 'POST':
        if report.status == 'submitted' or report.status == 'review_accepted':
            report.status = new_status
            ReportProcess.objects.create( 
                employee = request.user,
                emp_type = 'employee',
                report = report
                )
            if new_status == 'review_accepted':
                report.accepting_count += 1
            report.save()

        elif report.status == 'review_unaccepted':
            return HttpResponse('<br> <h2>This report was unaccepted..!</h2>')
        else:
            return HttpResponse('<br> <h2>This report was Done from the Leader..!</h2>')
    context = {}
    return render(request, 'report/review_report.html', context)

@login_required
@permission_required('report_manager.can_done_action',raise_exception=True, login_url='http://127.0.0.1:8000/admin/login/?next=/admin/')
def leader_action(request, report_id):
    report = Report.objects.get(id=report_id)
    new_status = request.POST.get("status")

    if request.method == 'POST':
        if report.status == 'submitted' or report.status == 'review_unaccepted':
            return HttpResponse('<br> <h2>This report did not review or unaccepted..!</h2>')
        elif report.status == 'approval' or report.status == 'rejected':
            return HttpResponse('<br> <h2>This report was Done from the Leader..!</h2>')
        else:
            report.status = new_status
            ReportProcess.objects.create( 
                employee = request.user,
                emp_type = 'leader',
                report = report
                )
            report.save()

            
    context = {}
    return render(request, 'report/leader_action.html', context)

class ReportList(generics.ListAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [permissions.IsAuthenticated]


    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class ReportDetails(generics.RetrieveAPIView):
    queryset = Report.objects.all()
    serializer_class = ReportSerializer
    permission_classes = [permissions.IsAuthenticated]

# --- Employee ---

class UserList(generics.ListAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [permissions.IsAuthenticated]


    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class UserDetail(generics.RetrieveAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeSerializer
    permission_classes = [permissions.IsAuthenticated]


