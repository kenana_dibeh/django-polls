from django.apps import AppConfig


class ReportManagerConfig(AppConfig):
    name = 'report_manager'
